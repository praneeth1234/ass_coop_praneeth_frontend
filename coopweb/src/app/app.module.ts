import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';


import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { SidebarComponent } from './sidebar/sidebar.component';
import { LoginComponent } from './login/login.component'
import { AuthService } from './services/auth.service';
import { AppRoutingModule } from './app-routing.module';
import { TokenInterceptor } from './shared/token-interceptor';
import { AuthGuard } from './shared/auth.guard';
import { AccountComponent } from './account/account.component';
import { AccountService } from './services/account.service';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    AccountComponent,
    SidebarComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    AppRoutingModule
  ],
  providers: [{
    provide:HTTP_INTERCEPTORS,
    useClass:TokenInterceptor,multi:true},
    
    AccountService,AuthService,AuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
