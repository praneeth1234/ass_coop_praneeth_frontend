import { Injectable } from "@angular/core";
import { AuthService } from '../services/auth.service';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate } from '@angular/router';
import { Observable } from 'rxjs';
import { promise } from 'protractor';

@Injectable()
export class AuthGuard implements CanActivate{
    path: ActivatedRouteSnapshot[];
    route: ActivatedRouteSnapshot;

constructor(private authService:AuthService ,private router:Router){}

canActivate(

    next:ActivatedRouteSnapshot,
    state:RouterStateSnapshot
):Observable<boolean>| Promise<boolean>|boolean
{
 if(this.authService.getToken()!=undefined){
return true;
 }  
 else{
this.router.navigate(['login'])
return false;
 } 
}

}