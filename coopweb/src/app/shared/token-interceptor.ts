import { Injectable } from "@angular/core";
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { error } from '@angular/compiler/src/util';


@Injectable()
export class TokenInterceptor implements HttpInterceptor{

constructor(private authservice:AuthService,private router:Router)
{
    }
intercept(request:HttpRequest<any>,next:HttpHandler):Observable<HttpEvent<any>>
{debugger;
    request=request.clone({
        setHeaders:{Authorization:`Bearer ${this.authservice.getToken()}`}
    });
return next.handle(request).pipe(tap((event:HttpEvent<any>)=>{
if(event instanceof HttpResponse)
{

}

},(err:any)=>{
if(err instanceof HttpErrorResponse)
{
  if(err.status==401) 
  {
   localStorage.removeItem("auth") ;
   this.router.navigate(['login']) ; 
  } 
}

}

))
}


}