import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  login: FormGroup;
  constructor(private authservice: AuthService,private router:Router) { }
  //metana constructor ekata authservice inject kra gnnawa, etakota me class eke ona tanaka eke method use krn puluwn
  ngOnInit() {

    this.login = new FormGroup({
      username: new FormControl(''),
      password: new FormControl('')

    });
  }
  onSubmit(formVal) {
    this.authservice.login(formVal).then((res) => {
//metana login eke hadapu method eke tynnet username,password, dan metana formbody eke formVal eket tynne e wagema Object, eka nisa eka metana use krn puluwn
      //navigate to product component
      this.router.navigate(['accounts']);
      console.log(res);
    }

    );
  }
}
