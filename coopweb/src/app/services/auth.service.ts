import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable()
export class AuthService {
    context: string = "users/";
    //backend eke user controll eke name users
    constructor(private httpClient: HttpClient) { }
     login(userData: Auth) {
        //metana Auth kynne auth.ts eke nama
        //backend ekata yawana tika metana wenne
        //login kyn method
        return this.httpClient.post<Token>(`${environment.BASE_URL}${this.context}authenticate`, userData).toPromise().then(res => {
            localStorage.setItem("auth", res.token);
            return true;
            // metana backend ekata html eke ewa yawanna ona,meken enne observable ekak, return wenawa value kihipayak, eka promises walata gannawa etakota return wenne ekai, eken passe then thanawa
            //eta passe local storage walta set kranawa
        }).catch(err => {
            return false;
        }

        )



    }
    getToken():string
    {
       return localStorage.getItem("auth");  
    }


}