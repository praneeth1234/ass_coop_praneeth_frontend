import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class AccountService {
  private accounts: Account[] = []
  context: string = "accounts"  

  constructor(private httpClient: HttpClient ) { }

  getAccounts():Account[]{
    return this.accounts;
  }

  getAccountsDB():Observable<Account[]>{
    return this.httpClient.get<Account[]>(environment.BASE_URL + this.context)
  }
   deleteProductByIDDB(id):Observable<boolean>{
    return this.httpClient.delete<boolean>(environment.BASE_URL + this.context +"/" + id);
  }



}




