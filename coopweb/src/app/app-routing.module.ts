import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { AuthGuard } from './shared/auth.guard';
import { AccountComponent } from './account/account.component';


const routes: Routes = [
 { path: '', redirectTo: '/login',pathMatch: 'full'},
 { path: 'accounts', component:AccountComponent,canActivate:[AuthGuard]},
 { path: "login", component:LoginComponent},
 { path: "**", component:LoginComponent,pathMatch: 'full'},
]
@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],exports:[RouterModule]
})
export class AppRoutingModule { }
//ng g module app-routing --flat --module=app  
//app yatate app-routing hadana command eka terminal